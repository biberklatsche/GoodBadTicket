package softwareforen.goodbadticket.jira.tester;

import softwareforen.goodbadticket.jira.tester.TicketVersionTester;
import com.atlassian.jira.rest.client.domain.Issue;
import org.junit.Assert;
import org.junit.Test;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public class TicketVersionTesterTest {
	@Test
	public void testGood_validVersion() {
		Issue issue = TestUtils.createTestIssueWithVersions("version");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketVersionTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_noVersions() {
		Issue issue = TestUtils.createTestIssueWithVersions();
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketVersionTester.test(ticket);
		Assert.assertEquals(Status.BAD, ticket.getResult().getStatus());
	}

}
