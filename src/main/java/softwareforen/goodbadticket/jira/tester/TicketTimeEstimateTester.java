package softwareforen.goodbadticket.jira.tester;

import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public final class TicketTimeEstimateTester {

	private static final int MAX_TIME_IN_MINUTES = 240;

	private TicketTimeEstimateTester() {
	}

	public static Ticket test(Ticket ticket) {
		if (ticket.getEstimateInMinutes() == 0) {
			ticket.getResult().setStatus(Status.BAD);
			ticket.getResult().addErrorDescription("Keine Schätzung.");
		} else if (ticket.getEstimateInMinutes() > MAX_TIME_IN_MINUTES) {
			ticket.getResult().setStatus(Status.BAD);
			ticket.getResult().addErrorDescription(
					"Mehr als 4 Stunden geschätzt.");
		} else {
			ticket.getResult().setStatus(Status.GOOD);
		}
		return ticket;
	}

}
