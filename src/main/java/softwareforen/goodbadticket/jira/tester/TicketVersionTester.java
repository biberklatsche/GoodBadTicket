package softwareforen.goodbadticket.jira.tester;

import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;

public final class TicketVersionTester {

	private TicketVersionTester() {
	}

	public static Ticket test(Ticket ticket) {
		if (ticket.hasVersion()) {
			ticket.setResultStatus(Status.GOOD);
		} else {
			ticket.setResultStatus(Status.BAD);
			ticket.addResultErrorDescription("Ticket keinem Sprint zugeordnet.");
		}
		return ticket;
	}
}
