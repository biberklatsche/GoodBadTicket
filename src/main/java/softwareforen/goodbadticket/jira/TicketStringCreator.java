package softwareforen.goodbadticket.jira;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public final class TicketStringCreator {

	private TicketStringCreator() {
	};

	public static List<String> createTicketList(String project,
			String ticketNumbersAsString) {
		if (isValid(project, ticketNumbersAsString)) {
			String normelizedString = ticketNumbersAsString;
			normelizedString = normelizedString.replaceAll(",(\\s)+", ",");
			normelizedString = normelizedString.replaceAll("(\\s)+,", ",");
			normelizedString = normelizedString.replaceAll("\\s+", ",");
			String[] splittedNumbers = normelizedString.split(",");
			List<String> ticketNumbers = new LinkedList<String>();
			for (String splittedNumber : splittedNumbers) {
				if (splittedNumber.contains("-")) {
					ticketNumbers
							.addAll(getNumberRange(project, splittedNumber));
				} else {
					ticketNumbers.add(String.format("%s-%s", project,
							splittedNumber));
				}

			}
			return ticketNumbers;
		} else {
			return Collections.emptyList();
		}
	}

	private static List<String> getNumberRange(String project,
			String rangeAsString) {
		String[] numbers = rangeAsString.split("-");
		int minValue = Integer.parseInt(numbers[0]);
		int maxValue = Integer.parseInt(numbers[1]);
		List<String> ticketNumbers = new LinkedList<String>();
		for (int value = minValue; value <= maxValue; value++) {
			ticketNumbers.add(String.format("%s-%d", project, value));
		}
		return ticketNumbers;
	}

	private static boolean isValid(String project, String ticketNumbersAsString) {
		return project != null && !project.isEmpty()
				&& ticketNumbersAsString != null
				&& !ticketNumbersAsString.isEmpty();
	}
}
