/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package softwareforen.goodbadticket.jira;

/**
 * 
 * @author lars-wolfram
 */
public enum Status {
	BAD("Nicht OK"), UKNOWN("Vielleicht OK"), GOOD("OK"), NONE("Unbekannt");

	Status(String description) {
		this.description = description;
	}

	private String description;

	@Override
	public String toString() {
		return description;
	}
}
