package softwareforen.goodbadticket.jira.tester;

import com.atlassian.jira.rest.client.domain.Issue;
import org.junit.Assert;
import org.junit.Test;
import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.Ticket;
import softwareforen.goodbadticket.jira.tester.TicketQaTester;

public class TicketQaTesterTest {
	@Test
	public void testUknown_noComments() {
		Issue issue = TestUtils.createTestIssueWithComments();
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.UKNOWN, ticket.getResult().getStatus());
	}

	@Test
	public void testUknown_noQAComments() {
		Issue issue = TestUtils.createTestIssueWithComments("- überprü...",
				"Was hab ich hier gemacht?");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.UKNOWN, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_qaComments() {
		Issue issue = TestUtils.createTestIssueWithComments("Für QA",
				"h3. qa erfolgreich\n - überprü...");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_whitespace() {
		Issue issue = TestUtils.createTestIssueWithComments("Für QA",
				"h3. qa   erfolgreich\n - überprü...");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_QaParentTicket() {
		Issue issue = TestUtils
				.createTestIssueWithComments("qa am oberticket\n ");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_QaParentTicket2() {
		Issue issue = TestUtils
				.createTestIssueWithComments("QA mit STR-4911\n ");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}

	@Test
	public void testGood_QaWith() {
		Issue issue = TestUtils
				.createTestIssueWithComments("QA zusammen mit\n ");
		Ticket ticket = new Ticket("STR-1", issue, "http://test");
		ticket = TicketQaTester.test(ticket);
		Assert.assertEquals(Status.GOOD, ticket.getResult().getStatus());
	}
}
