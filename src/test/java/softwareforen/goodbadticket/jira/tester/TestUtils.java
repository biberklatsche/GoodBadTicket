package softwareforen.goodbadticket.jira.tester;

import com.atlassian.jira.rest.client.domain.BasicStatus;
import com.atlassian.jira.rest.client.domain.Comment;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.TimeTracking;
import com.atlassian.jira.rest.client.domain.Version;
import java.util.ArrayList;
import java.util.List;
import org.mockito.Mockito;

public class TestUtils {
	static Issue createTestIssueWithDescription(String description) {
		Issue issue = Mockito.mock(Issue.class);
		Mockito.when(issue.getDescription()).thenReturn(description);
		return issue;
	}

	static Issue createTestIssueWithStatus(String name) {
		Issue issue = Mockito.mock(Issue.class);
		BasicStatus status = Mockito.mock(BasicStatus.class);
		Mockito.when(status.getName()).thenReturn(name);
		Mockito.when(issue.getStatus()).thenReturn(status);
		return issue;
	}

	static Issue createTestIssueWithVersions(String... versionNames) {
		Issue issue = Mockito.mock(Issue.class);
		List<Version> versions = new ArrayList<Version>(versionNames.length);
		for (String versionName : versionNames) {
			Version version = Mockito.mock(Version.class);
			Mockito.when(version.getName()).thenReturn(versionName);
			versions.add(version);
		}
		Mockito.when(issue.getFixVersions()).thenReturn(versions);
		return issue;
	}

	static Issue createTestIssueWithComments(String... commentsAsString) {
		Issue issue = Mockito.mock(Issue.class);
		List<Comment> comments = new ArrayList<Comment>(commentsAsString.length);
		for (String commentAsString : commentsAsString) {
			Comment comment = Mockito.mock(Comment.class);
			Mockito.when(comment.getBody()).thenReturn(commentAsString);
			comments.add(comment);
		}
		Mockito.when(issue.getComments()).thenReturn(comments);
		return issue;
	}

	static Issue createTestIssue(int estimateMinutes) {
		Issue issue = Mockito.mock(Issue.class);
		TimeTracking timeTracking = Mockito.mock(TimeTracking.class);
		Mockito.when(issue.getTimeTracking()).thenReturn(timeTracking);
		Mockito.when(timeTracking.getOriginalEstimateMinutes()).thenReturn(
				estimateMinutes);
		return issue;
	}

	static Issue createTestIssue(int estimateMinutes, int spentMinutes) {
		Issue issue = Mockito.mock(Issue.class);
		TimeTracking timeTracking = Mockito.mock(TimeTracking.class);
		Mockito.when(issue.getTimeTracking()).thenReturn(timeTracking);
		Mockito.when(timeTracking.getOriginalEstimateMinutes()).thenReturn(
				estimateMinutes);
		Mockito.when(timeTracking.getTimeSpentMinutes()).thenReturn(
				spentMinutes);
		return issue;
	}
}
