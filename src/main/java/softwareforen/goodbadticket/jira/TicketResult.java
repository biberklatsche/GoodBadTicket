package softwareforen.goodbadticket.jira;

public class TicketResult {
	private String ticketNumber;
	private String errorDescription = "";
	private Status status = Status.NONE;
	private String link = "";

	TicketResult(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticket) {
		this.ticketNumber = ticket;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		if (status != null && this.status.compareTo(status) > 0) {
			this.status = status;
		}
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link
	 *            the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}

	/**
	 * @param errorDescription
	 *            the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public void addErrorDescription(String errorDescription) {
		this.errorDescription = this.errorDescription + " " + errorDescription;
	}
}
