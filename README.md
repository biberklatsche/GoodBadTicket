Zum Repository "GoodBadTicket"
==============================

Das ist eine kleine Webapp zum überprüfen ob ein Ticket im Jira der Definition of done genügt oder nicht.
Geprüft wird:

1. Ist eine Beschreibung am Ticket vorhanden? (Es wird nur ein Kommentar erzeugt, Ticket ist dann trotzdem ok.)
2. Hat das Ticket den Status „Done“?
3. Hat das Ticket eine Zeitschätzung und ist diese kleiner als 4 Stunden?
4. Ist an dem Ticket Zeit gebucht und ist diese Buchung nicht mehr als 20% über der Schätzung?
5. Ist das Ticket einem Sprint zugeordnet?
6. Ist ein Kommentar mit dem Inhalt „Für QA“, „QA Hinweis“, „QA-TODO“ etc. vorhanden?
7. Ist ein Kommentar „QA erfolgreich“, „Keine QA notwendig“ etc. vorhanden?

Das ganze ist ein Maven-Projekt. Also einfach auschecken, mit mvn clean package bauen und auf einem beliebigen Tomcat installieren.
Unter http://<tomcat>/GoodBadTicket ist es dann verfügbar.

