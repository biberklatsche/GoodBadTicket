package softwareforen.goodbadticket.jira.data;

import java.util.LinkedList;
import java.util.List;

public class Results {
	private List<TicketResult> ticketResults = new LinkedList<TicketResult>();
	private int countBadTickets = 0;
	private int countGoodTickets = 0;
	private int countUknownTickets = 0;

	public List<TicketResult> getTicketResults() {
		return ticketResults;
	}

	public void addTicketResult(TicketResult ticketResult) {
		switch (ticketResult.getStatus()) {
		case GOOD:
			countGoodTickets++;
			break;
		case BAD:
			countBadTickets++;
			break;
		case UKNOWN:
			countUknownTickets++;
			break;
		}
		this.ticketResults.add(ticketResult);
	}

	public int getCountBadTickets() {
		return countBadTickets;
	}

	public void setCountBadTickets(int countBadTickets) {
		this.countBadTickets = countBadTickets;
	}

	public int getCountGoodTickets() {
		return countGoodTickets;
	}

	public void setCountGoodTickets(int countGoodTickets) {
		this.countGoodTickets = countGoodTickets;
	}

	public int getCountUknownTickets() {
		return countUknownTickets;
	}

	public void setCountUknownTickets(int countUknownTickets) {
		this.countUknownTickets = countUknownTickets;
	}
}
