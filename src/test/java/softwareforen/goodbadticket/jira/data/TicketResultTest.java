package softwareforen.goodbadticket.jira.data;

import softwareforen.goodbadticket.jira.data.Status;
import softwareforen.goodbadticket.jira.data.TicketResult;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TicketResultTest {

	@Test
	public void setStatusTest_noneToBad() {
		TicketResult result = new TicketResult("Tst");
		result.setStatus(Status.BAD);
		assertEquals(Status.BAD, result.getStatus());
	}

	@Test
	public void setStatusTest_noneToUknown() {
		TicketResult result = new TicketResult("Tst");
		result.setStatus(Status.UKNOWN);
		assertEquals(Status.UKNOWN, result.getStatus());
	}

	@Test
	public void setStatusTest_noneToGood() {
		TicketResult result = new TicketResult("Tst");
		result.setStatus(Status.GOOD);
		assertEquals(Status.GOOD, result.getStatus());
	}

	@Test
	public void setStatusTest_badToGood() {
		TicketResult result = new TicketResult("Tst");
		result.setStatus(Status.BAD);
		result.setStatus(Status.GOOD);
		assertEquals(Status.BAD, result.getStatus());
	}
}
